package ClassScore;
/**
 * 
 * @author asus
 *学生类 ，用来保存，输出和排序学生信息的。
 */


public class Student {

	private String S_name;
	private String S_sno;
	private int S_score;
	public Student(String s_name, String s_sno, int s_score) {
		super();
		S_name = s_name;
		S_sno = s_sno;
		S_score = s_score;
	}
	public Student() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getS_name() {
		return S_name;
	}
	public void setS_name(String s_name) {
		S_name = s_name;
	}
	public String getS_sno() {
		return S_sno;
	}
	public void setS_sno(String s_sno) {
		S_sno = s_sno;
	}
	public int getS_score() {
		return S_score;
	}
	public void setS_score(int s_score) {
		S_score = s_score;
	}
	@Override
	public String toString() {
		return S_sno + "," + S_name + "," + S_score;
	}
	
	public int compareTo(Student o) {
		//学生总分降序
		int score1 = o.getS_score() - this.getS_score();
		//总分相等时学号升序
		int  sno1= Integer.parseInt(this.getS_sno())- Integer.parseInt(o.getS_sno());
		
		return score1 == 0 ? sno1 : score1;
	}
	
}
