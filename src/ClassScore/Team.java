package ClassScore;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.jsoup.nodes.Element;
import org.jsoup.Jsoup;

public class Team {
	public static void main(String[] args) {
		try {
			//读取配置文件信息
			Properties properties = new Properties();
			properties.load(new FileInputStream("./resoures/config.properties"));
			//获取登入页面地址和cookie信息
			String url = properties.getProperty("url");	
			String cookie = properties.getProperty("cookie");
			//用connect的方法携带cookies连接到网页
			Document html = Jsoup.connect(url).header("Cookie", cookie).get();
			//创建一个学生类集合
			Map<String, Student> StuMap = new HashMap<String, Student>();
			ArrayList<Student> stuList = new ArrayList<Student>();
			
			//通过interaction-row获取各个活动的HTML代码
			Elements r_activities = html.getElementsByClass("interaction-row");
			int classSum=r_activities.size();
			//创建一个HashSet来保存url
			Set<String> urls = new HashSet<>();
			//获取该班课成员人数
			String stuNumber = html.getElementById("menu").select("a").get(1).select("span").get(1).text();
			int allStuSum = Integer.parseInt(stuNumber.substring(1,stuNumber.length()-1));
			Student[] allStudent=new Student[allStuSum];
			int cun =0;
			int aver=0;
			//通过循环获取所有url信息
			for (Element activity : r_activities) {
				if (activity.select("span[class~=^interaction-name]").text().contains("课堂完成")) {
					urls.add(activity.attr("data-url"));
				}
			}
			//对各活动的url进行解析
			for (String oneUrl : urls) {

				// 对活动的url地址进行解析获取源代码
				Document one = Jsoup.connect(oneUrl).header("Cookie", cookie).get();

				// 获取homework-item的标签
				Elements home_activities = one.getElementsByClass("homework-item");

				//获取学生信息
				if(cun==0) {
					allStudent =getOneStudent(home_activities,allStuSum);
					cun=1;
				}
				int homeSize=home_activities.size();
				//
				for(int j=0;j<homeSize;j++) {
					String stuSortText=home_activities.get(j).select("span").text();
					if(stuSortText.indexOf("未提交")!=-1 ||stuSortText.indexOf("尚无评分")!=-1) {}
					else{
						Elements stuScore=one.getElementsByClass("appraised-box cl ");
						String scoreStyle =stuScore.get(j).select("span").get(1).text();
						int score=isNum(scoreStyle);
						aver+=score;
						String name=home_activities.get(j).select("span").get(0).text();
						//判断是否是同个对象,是，则分数相加
						for(int s=0;s<allStuSum;s++) {
							if(name.equals(allStudent[s].getS_name())) {
								int sum=score+allStudent[s].getS_score();
								allStudent[s].setS_score(sum);
							}
						}
					}
				}
			}
			Arrays.sort(allStudent);
			for(int i=0;i<allStuSum;i++)
				System.out.println(allStudent[i]);
			File txt = new File("score.txt");
			//也可以使用BufferedWriter，将得到的数据写入txt文件中
			PrintWriter writeit = new PrintWriter(new FileWriter(txt));
			String totle = "最高经验值为:" + allStudent[0].getS_score() + "，" + "最低经验值为:" + allStudent[allStuSum - 1].getS_score() + "，"+ "平均经验值为:" + aver / allStuSum;	
			writeit.print(totle + "\r\n");
			for (Student stu : allStudent)
				writeit.print(stu.toString() + "\r\n");
			writeit.close();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	private static Student[] getOneStudent(Elements home_activities,int allStuSum) {
		
		Student[] students=new Student[allStuSum];
		for(int i=0;i<allStuSum;i++) {
			String name=home_activities.get(i).select("span").get(0).text();
			String id=home_activities.get(i).select("div").get(4).text();
			students[i]=new Student(name,id,0);
		}
		return students;

	}
	//提取分数的数字
	public static int isNum(String scorestr){
		String scoreStr="";
		for(int i=0;i<scorestr.length();i++){
			char chr=scorestr.charAt(i);
			if(chr>=48 && chr<=57)
				scoreStr+=chr;
		}
		int score=Integer.parseInt(scoreStr);
		return score;
	}
}
